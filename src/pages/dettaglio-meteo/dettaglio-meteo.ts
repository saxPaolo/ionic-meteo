import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';

import 'rxjs/add/operator/map';

/**
 * Generated class for the DettaglioMeteoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

/* prova di metodo com mappatura */
export class Meteo {
  main:string;
  desc:string;
}

// RICORDATI: IL DECORATORE PRECEDE IMMEDIATAMENTE LA CLASSE CHE DEVE DECORARE!!!!!!!
@Component({
  selector: 'page-dettaglio-meteo',
  templateUrl: 'dettaglio-meteo.html',
})

export class DettaglioMeteoPage {

  username:string = '';
  city:string = ''

  meteo:any = null;
  error:any = null;

  meteoMappata: Meteo = null;

  apiKey:any = '27b6b9504af9d03151e6894fd2e5fce7';

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
    this.city = this.navParams.get('city');
    this.username = this.navParams.get('username');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DettaglioMeteoPage');

    //this.city = this.navParams.get('city');

    this.getMeteo(this.city).subscribe (
      (response) => {
        this.meteo = response.json();
      },
      (error) => {
        this.error = error;
      }
    );

    this.getMeteoMappata(this.city).subscribe((m:Meteo) => {
      this.meteoMappata = m;
    });
  }

  getMeteo(city:string): Observable<Response> {
    let url = 'http://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=' + this.apiKey + '';
    return this.http.get(url);
  }

  getMeteoMappata(city:string): Observable<Meteo> {
    let url = 'http://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=' + this.apiKey + '';
    return this.http.get(url)
      .map((response) => {
        let m = response.json();
        let mf = new Meteo();
        // console.log(m);
        mf.main = m.weather[0].main;
        mf.desc = m.weather[0].description;

        return mf;
      });
  }
}