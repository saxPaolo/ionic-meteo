import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';

import 'rxjs/add/operator/map';

import { DettaglioMeteoPage } from '../dettaglio-meteo/dettaglio-meteo';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  username:string = "Domenico Sacchetti";
  city:string = '';

  people:any = null;

  constructor(public navCtrl: NavController, public http: Http) {

  }

  onMostraMeteo() {
    this.navCtrl.push(DettaglioMeteoPage, {
      city: this.city,
      username: this.username
    });
  }

  ionViewDidLoad() {
    this.getSWAPIpeople().subscribe((response) => {
      this.people = response.json();
    });
  }

  /* https://swapi.co/api/people */
  getSWAPIpeople(): Observable<Response> {
    let url = 'https://swapi.co/api/people';
    return this.http.get(url);
  }

  onNextPage () {
    if(this.people && this.people.next) {
      this.http.get(this.people.next).subscribe((response) => {
        this.people = response.json();
      })
    }
  }
  
  onPreviousPage () {
    if(this.people && this.people.previous) {
      this.http.get(this.people.previous).subscribe((response) => {
        this.people = response.json();
      })
    }
  }
}
